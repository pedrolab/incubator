#!/bin/sh -e

owrt_img="/path/to/image"

echo "__flash_openwrt_sysupgrade --openwrt_sysupgrade_image ${owrt_img}" | cdist/bin/cdist config -c . -g cdist.cfg \
  --remote-exec ./remote-exec-owrt.sh \
  --remote-copy ./remote-copy-owrt.sh \
  -vv -i - "${@}"
#  -vvvvvv -i - "${@}"
