#!/bin/sh -e

hostname=192.168.1.20
timeout=500

# Get epoch, see: https://stackoverflow.com/a/12746260
begin="$(awk 'BEGIN{srand();print srand()}')"
delta=0
echo h1
while [ ${timeout} -lt 0 ] || [ $delta -lt ${timeout} ]; do
    echo h2
	# Figure out if the host is "up" or down
	if nc -z -w 1 "${hostname}" 22 2>&1 > /dev/null; then
		CURRENT_STATE="UP"
	else
		CURRENT_STATE="DOWN"
		# Remember this has been down
		HAS_BEEN_DOWN="YES"
	fi
	# If state matches what we expect, we're done
	if [ "${CURRENT_STATE}" = "UP" ] && \
	   [ -n "${HAS_BEEN_DOWN}" ]; then
		FINISHED="YES"
		break;
	fi
	# This is here in case hostname is a router
	# in that case, we might get 'no route to host' or similar
	# and fail immediately instead of waiting 1 sec with nc
	sleep 1
	now="$(awk 'BEGIN{srand();print srand()}')"
	delta="$((${now}-${begin}))"
    echo "delta ${delta}"
done

if [ -z "${FINISHED}" ]; then
	# This means we timed out
	echo 'Timed out waiting for ${hostname} to reboot' >> /dev/stderr
	# Exit with failure
	exit 1
fi

