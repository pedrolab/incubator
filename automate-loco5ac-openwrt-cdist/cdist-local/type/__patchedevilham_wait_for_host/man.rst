cdist-type__evilham_wait_for_host(7)
============================

NAME
----
cdist-type__evilham_wait_for_host - Wait for a remote host to be up


DESCRIPTION
-----------
This type waits for a remote host to be "up", where up is defined as:

- Reachable from the controller host to port 22/tcp.

This type does not actually reboot a remote host, that's because during tests
cdist didn't quite behave properly in those cases, so the reboot must come
from `gencode-remote` in some other type.

Always take care not to reboot your own controller machine and be extra careful
when doing these things on your routers :-D.


OPTIONAL PARAMETERS
-------------------
await-seconds
    How many seconds to wait after determining `--hostname` to be up.
    This delay prevents a slow-to-start host from maybe refusing connections.
    Defaults to 2 seconds.

timeout-seconds
    How many seconds to wait for `--hostname` to be up.
    Defaults to 600 seconds.

hostname
    If present, the hostname to wait for.
    If not passed, `__target_host` will be used.


EXAMPLES
--------

.. code-block:: sh

    # Test things services are not affected by a reboot
    __evilham_wait_for_host 'random-reboot'


SEE ALSO
--------
- `shutdown(8)`


AUTHORS
-------
Evilham <contact@evilham.com>


COPYING
-------
Copyright \(C) 2021 Evilham.
