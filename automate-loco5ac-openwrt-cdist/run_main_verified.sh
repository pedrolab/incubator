#!/bin/sh -e

if [ ! -d "cdist" ]; then
  git clone https://code.ungleich.ch/ungleich-public/cdist.git
  cd cdist
  git checkout defa3c22eaef88652a2bec6135d038a84710c41e

  mypatch="$(cat <<PATCH
diff --git a/cdist/conf/explorer/os b/cdist/conf/explorer/os
index 46d87f3e..7a8c5fe4 100755
--- a/cdist/conf/explorer/os
+++ b/cdist/conf/explorer/os
@@ -156,5 +156,4 @@ if [ -f /etc/os-release ]; then
    exit 0
 fi

-echo "Unknown OS" >&2
-exit 1
+echo unknown
PATCH
)"
  git apply 2>/dev/null <<APPLY
${mypatch}
APPLY

  cd ..
fi

if [ ! -d "cdist-evilham" ]; then
  git clone https://git.sr.ht/~evilham/cdist-evilham
  cd cdist-evilham
  git checkout 00494605f93fb9f013845bd89677966df48435a7
  cd ..
fi

# this is a valid way to execute the type
# this way I can limit the cdist command because. inside of a manifest, would be `timeout 15 __ubnt_prepare_env`
cdist/bin/cdist config -c . -g cdist.cfg \
  --remote-exec ./remote-exec-ubnt.sh \
  --remote-copy ./remote-copy-ubnt.sh \
  -vv -i ./manifest_test 192.168.1.20
#  -vvvvvv -i ./manifest 192.168.1.20
