good news, this project started as an experimented called *automate-loco5ac-openwrt-cdist* now is evolving to a cdist facilities to run administration tasks of openwrt devices for openwrt

use at your own risk

# cdist: flash openwrt sysupgrade image

it uploads the target image on your device and flashes it **without preserving configuration** (`sysupgrade -n` option)

use the following script:

```
./flash_device_with_openwrt_sysupgrade_image.sh yourhost
```

beware that you need to edit the script to include the openwrt sysupgrade image local path in your system in the variable `owrt_img`

# Automate Loco5AC openwrt cdist

there are two ways to execute it:

- `run_main_verified.sh` : runs the way I tested and it worked
- `run_main_rolling_release.sh`: useful when you want the latest version of git repos but still apply these patches (hence, more experimental and buggy)

## howt it works / looking at the code

it basically does [this](https://openwrt.org/toh/ubiquiti/common#wa_v870xc_v870) but automatically with the help of [cdi.st](https://cdi.st). The only requirement (to avoid losing the connection) is having a router between the target device and your laptop

Essentially, these are the most interesting files that make it work (4 files of ~10 lines of code)

- type `__prepare_ubnt_for_openwrt`
  - [__prepare_ubnt_for_openwrt/manifest](https://gitlab.com/pedrolab/incubator/-/blob/master/automate-loco5ac-openwrt-cdist/cdist-local/type/__prepare_ubnt_for_openwrt/manifest)
  - [__prepare_ubnt_for_openwrt/gencode-remote](https://gitlab.com/pedrolab/incubator/-/blob/master/automate-loco5ac-openwrt-cdist/cdist-local/type/__prepare_ubnt_for_openwrt/gencode-remote)
- type `__flash_openwrt_on_ubnt`
  - [__flash_openwrt_on_ubnt/manifest](https://gitlab.com/pedrolab/incubator/-/blob/master/automate-loco5ac-openwrt-cdist/cdist-local/type/__flash_openwrt_on_ubnt/manifest)
  - [__flash_openwrt_on_ubnt/gencode-remote](https://gitlab.com/pedrolab/incubator/-/blob/master/automate-loco5ac-openwrt-cdist/cdist-local/type/__flash_openwrt_on_ubnt/gencode-remote)

## changes on cdist

### custom remote exec and remote copy

[remote-copy-ubnt.sh](https://gitlab.com/pedrolab/incubator/-/blob/master/automate-loco5ac-openwrt-cdist/remote-copy-ubnt.sh) and [remote-exec-ubnt.sh](https://gitlab.com/pedrolab/incubator/-/blob/master/automate-loco5ac-openwrt-cdist/remote-exec-ubnt.sh) help on:

- have a custom non-root user (ubnt)
- use `sshpass` (which is installed with `apt install sshpass`)
- optimization: used ssh multiplexing (from 30 sec to 3 sec)
  - ref1 https://www.cdi.st/manual/latest/cdist-best-practice.html#speeding-up-ssh-connections
  - ref2 https://en.wikibooks.org/wiki/OpenSSH/Cookbook/Multiplexing#Setting_Up_Multiplexing
- extra references about remote exec and remote copy
  - https://www.cdi.st/manual/6.9.1/cdist-remote-exec-copy.html#remote-exec-and-copy-commands
  - `$@` is important -> https://code.ungleich.ch/ungleich-public/cdist/-/blob/master/other/examples/remote/ssh/copy

### patch on __file type

type `__file` was patched to use `md5sum` instead of `cksum`, which is not available nor installable in that firmware. The patched type is [__file_md5](https://gitlab.com/pedrolab/incubator/-/tree/master/automate-loco5ac-openwrt-cdist/cdist-local/type/__file_md5)

### patch os explorer

cdist/conf/explorer/os

allow working with an unknown operating system

[check the patch here](https://gitlab.com/pedrolab/incubator/-/blob/master/automate-loco5ac-openwrt-cdist/run_main_verified.sh#L8-21)

## TODO

- manifest_test file cannot be manifest (?)
- allow download / automate hashes
  - note: firmware is 21.02.0-rc1 for Ubiquiti Nanostation AC loco with hash 18165dd068f3ee88b32d01fd924b8118ed4ac1c98e0e89086700586033b5a184
