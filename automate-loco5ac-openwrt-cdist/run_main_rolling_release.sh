#!/bin/sh -e

update_cdist_repo() {
  cd dist
  git pull --rebase --autostash

  mypatch="$(cat <<PATCH
diff --git a/cdist/conf/explorer/os b/cdist/conf/explorer/os
index 46d87f3e..7a8c5fe4 100755
--- a/cdist/conf/explorer/os
+++ b/cdist/conf/explorer/os
@@ -156,5 +156,4 @@ if [ -f /etc/os-release ]; then
    exit 0
 fi

-echo "Unknown OS" >&2
-exit 1
+echo unknown
PATCH
)"

  (
  git apply 2>/dev/null <<APPLY
${mypatch}
APPLY
  ) ||
  (
  git apply -R <<APPLY
${mypatch}
APPLY
  git apply <<APPLY
${mypatch}
APPLY
  )
  cd ..
}

if [ ! -d "cdist" ]; then
  git clone https://code.ungleich.ch/ungleich-public/cdist.git
  cd cdist

  mypatch="$(cat <<PATCH
diff --git a/cdist/conf/explorer/os b/cdist/conf/explorer/os
index 46d87f3e..7a8c5fe4 100755
--- a/cdist/conf/explorer/os
+++ b/cdist/conf/explorer/os
@@ -156,5 +156,4 @@ if [ -f /etc/os-release ]; then
    exit 0
 fi

-echo "Unknown OS" >&2
-exit 1
+echo unknown
PATCH
)"
  git apply 2>/dev/null <<APPLY
${mypatch}
APPLY

  cd ..
fi

update_cdist_repo

if [ ! -d "cdist-evilham" ]; then
  git clone https://git.sr.ht/~evilham/cdist-evilham
  git pull
fi

# this is a valid way to execute the type
# this way I can limit the cdist command because. inside of a manifest, would be `timeout 15 __ubnt_prepare_env`
cdist/bin/cdist config -c . -g cdist.cfg \
  --remote-exec ./remote-exec-ubnt.sh \
  --remote-copy ./remote-copy-ubnt.sh \
  -vv -i ./manifest_test 192.168.1.20
#  -vvvvvv -i ./manifest 192.168.1.20
