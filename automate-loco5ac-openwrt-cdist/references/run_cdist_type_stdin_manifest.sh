#!/bin/sh -e

echo '    this is how I started, but it is incomplete'
echo '    better run run_cdist_type_real_manifest.sh'
exit 1

update_cdist_repo() {
  cd dist
  #git pull --rebase --autostash

  mypatch="$(cat <<PATCH
diff --git a/cdist/conf/explorer/os b/cdist/conf/explorer/os
index 46d87f3e..7a8c5fe4 100755
--- a/cdist/conf/explorer/os
+++ b/cdist/conf/explorer/os
@@ -156,5 +156,4 @@ if [ -f /etc/os-release ]; then
    exit 0
 fi

-echo "Unknown OS" >&2
-exit 1
+echo unknown
PATCH
)"

  (
  git apply 2>/dev/null <<APPLY
${mypatch}
APPLY
  ) ||
  (
  git apply -R <<APPLY
${mypatch}
APPLY
  git apply <<APPLY
${mypatch}
APPLY
  )
  cd ..
}

if [ ! -d "cdist" ]; then
  git clone https://code.ungleich.ch/ungleich-public/cdist.git
  cd cdist
  git checkout defa3c22eaef88652a2bec6135d038a84710c41e

  mypatch="$(cat <<PATCH
diff --git a/cdist/conf/explorer/os b/cdist/conf/explorer/os
index 46d87f3e..7a8c5fe4 100755
--- a/cdist/conf/explorer/os
+++ b/cdist/conf/explorer/os
@@ -156,5 +156,4 @@ if [ -f /etc/os-release ]; then
    exit 0
 fi

-echo "Unknown OS" >&2
-exit 1
+echo unknown
PATCH
)"
  git apply 2>/dev/null <<APPLY
${mypatch}
APPLY

  cd ..
fi

# enable next line only if you want to update frequently
#update_cdist_repo

if [ ! -d "cdist-evilham" ]; then
  git clone https://git.sr.ht/~evilham/cdist-evilham
  cd cdist-evilham
  git checkout 00494605f93fb9f013845bd89677966df48435a7
  cd ..
fi

# this is a valid way to execute the type
# this way I can limit the cdist command because. inside of a manifest, would be `timeout 15 __ubnt_prepare_env`

#echo "__prepare_ubnt_for_openwrt" | timeout 1500 cdist/bin/cdist config -c cdist-local -c cdist-evilham \
#echo "__prepare_ubnt_for_openwrt" | cdist/bin/cdist config -c . \
echo "__ping" | cdist/bin/cdist config -c . -g cdist.cfg \
  --remote-exec ./remote-exec-ubnt.sh \
  --remote-copy ./remote-copy-ubnt.sh \
  -vv -i - 192.168.1.20
#  -vvvvvv -i - 192.168.1.20
