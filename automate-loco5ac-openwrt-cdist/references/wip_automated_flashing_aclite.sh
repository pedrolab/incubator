#!/bin/sh -e

cat <<EOF
    this script is just a reference of doing it without cdist
    the instructions for nanostationloco are wrong
    it comes from another script used for ubiquiti aclite flashing procedure
EOF
exit 1

## script used to flash devices
## still a litte bit hardcoded
## but reached 5 min per antenna (nice!)

ssh_ubnt() {
  local cmd="$1"
  sshpass -p 'ubnt' ssh ubnt@192.168.1.20 "$cmd"
}

scp_ubnt() {
  local src="$1"
  local dst="$2"
  sshpass -p 'ubnt' scp "$src" ubnt@192.168.1.20:"$dst"
}

section() {
  echo "section $1"
  echo 'press any key to continue'
  # thanks https://stackoverflow.com/questions/15744421/read-command-doesnt-wait-for-input
  read _
}

wait_device() {
  local t_addr="$1"
  local state="$2"

  if [ ! "$state" = "off" ]; then
    state="on"
  fi

  #sleep 10

  # https://askubuntu.com/questions/929659/bash-wait-for-a-ping-success
  printf "waiting device to be $state "
  while true; do
    if timeout 0.2 ping -c 1 -n "$t_addr" 2>&1 > /dev/null; then
      if [ "$state" = "on" ]; then
        break
      fi
    else
      if [ "$state" = "off" ]; then
        break
      fi
    fi
    printf "."
    sleep 1
  done

  printf "\n  device is $state\n" > /dev/stderr
  aplay '/home/user/sounds/ding1.wav' > /dev/null 2>&1
  sleep 10
}

main() {
  local t_file1='/home/user/temp/3.7.58/ubnt.bin'
  local sha_file1='850921008ffa17450dd3ca235ab933a7d50e048bc103e8b27c78ddb35578be6a9a50fb20194a3198913ed370872142ae5f93e0026d84cc299b558ff8073cc6e7'
  #local t_file2='/home/user/temp/2020-11-4-flashing/openwrt-19.07.4-ath79-generic-ubnt_unifiac-lite-squashfs-sysupgrade.bin'
  local t_file2='/home/user/temp/maskedloc/maskedloc_2021-01-05-01-14-319/maskedloc-factory.bin'

  if [ ! -f "$t_file1" ]; then
    wget https://web.archive.org/web/20201011085042/https://dl.ui.com/unifi/firmware/U7PG2/3.7.58.6385/BZ.qca956x.v3.7.58.6385.170508.0957.bin -O "$t_file1"
  fi
  if [ "$(sha256sum $t_file1 | cut -f1 -d' ')" = "$sha_file1" ]; then
    echo 'sha256 mismatch'
    exit 1
  fi

  echo "1. upload factory firmware 3.7.58"
  wait_device "192.168.1.20" "on"
  scp_ubnt "$t_file1" "/tmp/ubnt.bin"
  # thanks https://stackoverflow.com/questions/29142/getting-ssh-to-execute-a-command-in-the-background-on-target-machine
  ssh_ubnt "( ( fwupdate.real -m /tmp/ubnt.bin > /dev/null 2>&1 ) & )"
  wait_device "192.168.1.20" "off"
  #alt method (tftp recovery) - not tested in this script
  #atftp --trace --option "timeout 1" --option "mode octet" --put --local-file "$t_file1" 192.168.1.20

  echo "2. upload openwrt image"
  wait_device "192.168.1.20"
  scp_ubnt "$t_file2" "/tmp/ubnt.bin"
  ssh_ubnt "mtd write /tmp/ubnt.bin kernel0"
  ssh_ubnt "mtd erase kernel1"
  if [ "$(ssh_ubnt "cat /proc/mtd | grep mtd4 | cut -f4 -d' '")" = \"bs\" ]; then
    ssh_ubnt "dd if=/dev/zero bs=1 count=1 of=/dev/mtd4"
  else
    echo 'mtd4 is not equal to bs as stated in doc'
    exit 1
  fi

  ssh_ubnt reboot

  wait_device "192.168.1.1" "on"
  echo done!
}

main
