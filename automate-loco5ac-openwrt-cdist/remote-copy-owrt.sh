#!/bin/sh -e

# apt install sshpass
# slow
#sshpass -p ubnt scp -o User=ubnt -q "$@"
# optimization
#sshpass -p ubnt scp -o User=ubnt -o ControlMaster=auto -o ControlPath=/tmp/%r@%h:%p -o ControlPersist=2h -q "$@"
scp -o ControlMaster=auto -o ControlPath=/tmp/%r@%h:%p -o ControlPersist=2h -q "$@"
