this wants to be a peertube type for cdist

meanwhile, it installs peertube v4.0 (tested for debian10)

you have to load your data according to your environment (check myenv.example and copy it to myenv)

extra note: `apt install emacs-nox` emacs is useful with its M-x shell thing to analyze long output

## prepare a new version

use script `_update_peertube_configurations.sh`

## update/maintenance

- npm install -g npm

## TODO

optional nginx part is untested
