#!/bin/sh

set -eux

. ./myenv

# ~minify
cat myenv local-production.yaml.sh nginx.sh install_peertube.sh | grep -v '#!/bin/sh' | sed '1i #!/bin/sh -e'  | ssh "${TARGET_HOST}" -- sh -e
