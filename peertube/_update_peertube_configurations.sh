#!/bin/sh

set -e
set -u

# inspiration https://code.ungleich.ch/ungleich-public/cdist-contrib/src/branch/master/type/__jitsi_meet_domain/files/_update_jitsi_configurations.sh

get_url() {
  file="${1}"
  printf "%s/raw/%s/%s" "${REPO}" "${BRANCH}" "${file}"
}

download_file() {
  file="${1}"
  destination="${2:-${file}.sh.orig}"
  url="$(get_url "${file}")"
  echo "Downloading ${destination} - ${url}"
  curl -s -L "${url}" -z  "${destination}" -o "${destination}"
}

# function that uses the vars of patch_config function
regen_diff() {
  diff -u "${src_file}" "${dst_file}" > "${patch_file}" || true

  # override diff header
  sed -i '1,2d' "${patch_file}"
  sed -i "1i --- ${target_file}" "${patch_file}"
  sed -i "1i +++ ${target_file}" "${patch_file}"
}

patch_config() {
  file_arg="${1}"
  src_file="${1}.orig"
  dst_file="${1}.sh"
  patch_file="${1}.patch"
  target_file="${1}.sh"

  if [ "${REGEN_DIFF:-}" ]; then
    regen_diff
  fi
  cp  "${src_file}" "${target_file}"
  # src https://unix.stackexchange.com/questions/423186/diff-how-to-ignore-empty-lines
  patch --merge -i "${patch_file}"
}

main() {

  if [ "${DEBUG:-}" ]; then
    set -x
  fi

  # src https://github.com/Chocobozzz/PeerTube/tags
  . ./myenv
  BRANCH="${VERSION}"
  REPO="https://github.com/Chocobozzz/PeerTube"

  download_file 'support/nginx/peertube' 'nginx.orig'
  patch_config 'nginx'

  download_file 'config/production.yaml.example' 'production.yaml.orig'
  #patch_config 'production.yaml'
}

main "${@}"
