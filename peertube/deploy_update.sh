#!/bin/sh -e

. ./myenv

# ~minify
cat myenv production.yaml.sh nginx.sh update_peertube.sh | grep -v '#!/bin/sh' | sed '1i #!/bin/sh -e'  | ssh "${TARGET_HOST}" -- sh -e
