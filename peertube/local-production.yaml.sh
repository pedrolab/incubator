#!/bin/sh -e

PT_CONFIG="$(cat <<EOF
# template for version ${PT_VERSION}

# Correspond to your reverse proxy server_name/listen configuration (i.e., your public PeerTube instance URL)
webserver:
  https: true
  hostname: '${WEBSERVER_HOST}'
  port: 443

# Secrets you need to generate the first time you run PeerTube
secrets:
  # Generate one using `openssl rand -hex 32`
  peertube: '$(openssl rand -hex 32)'

# SMTP server to send emails
smtp:
  # smtp or sendmail
  transport: smtp
  # Path to sendmail command. Required if you use sendmail transport
  sendmail: null
  hostname: ${SMTP_HOST}
  port: ${SMTP_PORT} # StartTLS: 587 / TLS: 465
  username: ${SMTP_USER}
  password: ${SMTP_PASSWD}
  tls: ${SMTP_TLS} # If you use StartTLS: false
  disable_starttls: false
  ca_file: null # Used for self signed certificates
  from_address: '${SMTP_FROM}'

# From the project root directory
storage:
  tmp: '${PEERTUBE_STORAGE}/storage/tmp/' # Use to download data (imports etc), store uploaded files before processing...
  bin: '${PEERTUBE_STORAGE}/storage/bin/'
  avatars: '${PEERTUBE_STORAGE}/storage/avatars/'
  videos: '${PEERTUBE_STORAGE}/storage/videos/'
  streaming_playlists: '${PEERTUBE_STORAGE}/storage/streaming-playlists/'
  redundancy: '${PEERTUBE_STORAGE}/storage/redundancy/'
  logs: '${PEERTUBE_STORAGE}/storage/logs/'
  previews: '${PEERTUBE_STORAGE}/storage/previews/'
  thumbnails: '${PEERTUBE_STORAGE}/storage/thumbnails/'
  torrents: '${PEERTUBE_STORAGE}/storage/torrents/'
  captions: '${PEERTUBE_STORAGE}/storage/captions/'
  cache: '${PEERTUBE_STORAGE}/storage/cache/'
  plugins: '${PEERTUBE_STORAGE}/storage/plugins/'
  # Overridable client files : logo.svg, favicon.png and icons/*.png (PWA) in client/dist/assets/images
  # - logo.svg
  # - favicon.png
  # - default-playlist.jpg
  # - default-avatar-account.png
  # - default-avatar-video-channel.png
  # - and icons/*.png (PWA)
  # Could contain for example assets/images/favicon.png
  # If the file exists, peertube will serve it
  # If not, peertube will fallback to the default file
  client_overrides: '${PEERTUBE_STORAGE}/storage/client-overrides/'

admin:
  # Used to generate the root user at first startup
  # And to receive emails from the contact form
  email: '${ADMIN_EMAIL}'
EOF
)"
