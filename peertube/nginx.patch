+++ nginx.sh
--- nginx.sh
@@ -1,3 +1,8 @@
+#!/bin/sh -e
+
+# template for v3.0.1
+
+NGINX_CONFIG="$(cat <<EOF
 # Minimum Nginx version required:  1.13.0 (released Apr 25, 2017)
 # Please check your Nginx installation features the following modules via 'nginx -V':
 # STANDARD HTTP MODULES: Core, Proxy, Rewrite, Access, Gzip, Headers, HTTP/2, Log, Real IP, SSL, Thread Pool, Upstream, AIO Multithreading.
@@ -12,7 +17,7 @@
     default_type "text/plain";
     root /var/www/certbot;
   }
-  location / { return 301 https://$host$request_uri; }
+  location / { return 301 https://\$host\$request_uri; }
 }
 
 upstream backend {
@@ -31,8 +36,8 @@
   # Certificates
   # you need a certificate to run in production. see https://letsencrypt.org/
   ##
-  ssl_certificate     /etc/letsencrypt/live/${WEBSERVER_HOST}/fullchain.pem;
-  ssl_certificate_key /etc/letsencrypt/live/${WEBSERVER_HOST}/privkey.pem;
+  ssl_certificate     ${TLS_CERT};
+  ssl_certificate_key ${TLS_KEY};
 
   location ^~ '/.well-known/acme-challenge' {
     default_type "text/plain";
@@ -60,9 +65,9 @@
   ##
 
   location @api {
-    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
-    proxy_set_header Host            $host;
-    proxy_set_header X-Real-IP       $remote_addr;
+    proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
+    proxy_set_header Host            \$host;
+    proxy_set_header X-Real-IP       \$remote_addr;
 
     client_max_body_size  100k; # default is 1M
 
@@ -111,10 +116,10 @@
 
   location @api_websocket {
     proxy_http_version 1.1;
-    proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
-    proxy_set_header   Host            $host;
-    proxy_set_header   X-Real-IP       $remote_addr;
-    proxy_set_header   Upgrade         $http_upgrade;
+    proxy_set_header   X-Forwarded-For \$proxy_add_x_forwarded_for;
+    proxy_set_header   Host            \$host;
+    proxy_set_header   X-Real-IP       \$remote_addr;
+    proxy_set_header   Upgrade         \$http_upgrade;
     proxy_set_header   Connection      "upgrade";
 
     proxy_pass http://backend;
@@ -172,24 +177,24 @@
 
   # Bypass PeerTube for performance reasons. Optional.
   # Should be consistent with client-overrides assets list in /server/controllers/client.ts
-  location ~ ^/client/(assets/images/(icons/icon-36x36\.png|icons/icon-48x48\.png|icons/icon-72x72\.png|icons/icon-96x96\.png|icons/icon-144x144\.png|icons/icon-192x192\.png|icons/icon-512x512\.png|logo\.svg|favicon\.png|default-playlist\.jpg|default-avatar-account\.png|default-avatar-video-channel\.png))$ {
+  location ~ ^/client/(assets/images/(icons/icon-36x36\.png|icons/icon-48x48\.png|icons/icon-72x72\.png|icons/icon-96x96\.png|icons/icon-144x144\.png|icons/icon-192x192\.png|icons/icon-512x512\.png|logo\.svg|favicon\.png|default-playlist\.jpg|default-avatar-account\.png|default-avatar-video-channel\.png))\$ {
     add_header Cache-Control "public, max-age=31536000, immutable"; # Cache 1 year
 
     root /var/www/peertube;
 
-    try_files /storage/client-overrides/$1 /peertube-latest/client/dist/$1 @api;
+    try_files /storage/client-overrides/\$1 /peertube-latest/client/dist/\$1 @api;
   }
 
   # Bypass PeerTube for performance reasons. Optional.
   location ~ ^/client/(.*\.(js|css|png|svg|woff2|otf|ttf|woff|eot))$ {
     add_header Cache-Control "public, max-age=31536000, immutable"; # Cache 1 year
 
-    alias /var/www/peertube/peertube-latest/client/dist/$1;
+    alias /var/www/peertube/peertube-latest/client/dist/\$1;
   }
 
   # Bypass PeerTube for performance reasons. Optional.
   location ~ ^/static/(thumbnails|avatars)/ {
-    if ($request_method = 'OPTIONS') {
+    if (\$request_method = 'OPTIONS') {
       add_header Access-Control-Allow-Origin  '*';
       add_header Access-Control-Allow-Methods 'GET, OPTIONS';
       add_header Access-Control-Allow-Headers 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
@@ -204,9 +209,9 @@
     add_header Access-Control-Allow-Headers   'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
     add_header Cache-Control                  "public, max-age=7200"; # Cache response 2 hours
 
-    rewrite ^/static/(.*)$ /$1 break;
+    rewrite ^/static/(.*)$ /\$1 break;
 
-    try_files $uri @api;
+    try_files \$uri @api;
   }
 
   # Bypass PeerTube for performance reasons. Optional.
@@ -214,19 +219,19 @@
     limit_rate_after            5M;
 
     # Clients usually have 4 simultaneous webseed connections, so the real limit is 3MB/s per client
-    set $peertube_limit_rate    800k;
+    set \$peertube_limit_rate    800k;
 
     # Increase rate limit in HLS mode, because we don't have multiple simultaneous connections
-    if ($request_uri ~ -fragmented.mp4$) {
-      set $peertube_limit_rate  5M;
+    if (\$request_uri ~ -fragmented.mp4$) {
+      set \$peertube_limit_rate  5M;
     }
 
     # Use this line with nginx >= 1.17.0
-    #limit_rate $peertube_limit_rate;
+    #limit_rate \$peertube_limit_rate;
     # Or this line if your nginx < 1.17.0
-    set $limit_rate $peertube_limit_rate;
+    set \$limit_rate \$peertube_limit_rate;
 
-    if ($request_method = 'OPTIONS') {
+    if (\$request_method = 'OPTIONS') {
       add_header Access-Control-Allow-Origin  '*';
       add_header Access-Control-Allow-Methods 'GET, OPTIONS';
       add_header Access-Control-Allow-Headers 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
@@ -236,7 +241,7 @@
       return 204;
     }
 
-    if ($request_method = 'GET') {
+    if (\$request_method = 'GET') {
       add_header Access-Control-Allow-Origin  '*';
       add_header Access-Control-Allow-Methods 'GET, OPTIONS';
       add_header Access-Control-Allow-Headers 'Range,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type';
@@ -254,6 +259,8 @@
     rewrite ^/static/webseed/(.*)$ /videos/$1 break;
     rewrite ^/static/(.*)$         /$1        break;
 
-    try_files $uri @api;
+    try_files \$uri @api;
   }
 }
+EOF
+)"
