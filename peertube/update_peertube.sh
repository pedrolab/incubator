#!/bin/sh -e

load_env() {
  file="${1}"
  if [ -f file ]; then
    . "${file}"
  fi
}

if [ "$(dirname $0)" != '.' ]; then
  cd "$(dirname $0)"
fi

# force time update (because it could come from a rollback)
NO_NTP="true"
if [ -z "$NO_NTP" ]; then
  sudo service ntp stop
  sudo ntpd -gq
  sudo service ntp start
  # give some time
  #    E: Could not get lock /var/lib/dpkg/lock-frontend - open (11: Resource temporarily unavailable)
  #    E: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), is another process using it?
  sleep 2
fi

# load env vars
#   and ensure non empty vars in templates
set -u
load_env ./myenv
load_env ./production.yaml.sh
load_env ./nginx.sh
set +u

# https://serverfault.com/questions/227190/how-do-i-ask-apt-get-to-skip-any-interactive-post-install-configuration-steps
export DEBIAN_FRONTEND=noninteractive

# general ref https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/production.md

# dependencies https://github.com/Chocobozzz/PeerTube/blob/develop/support/doc/dependencies.md
apt update
# On a fresh Debian/Ubuntu, as root user, install basic utility programs needed for the installation
apt install -yq curl sudo unzip vim
. /etc/os-release
if [ ${VERSION_ID} -ge 11 ]; then
  apt install python-is-python3
fi
# It would be wise to disable root access and to continue this tutorial with a user with sudoers group access
# Install NodeJS 12.x -> src https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
wget https://deb.nodesource.com/setup_12.x
bash setup_12.x
apt install -yq nodejs
# Install yarn, and be sure to have a recent version https://yarnpkg.com/en/docs/install#linux-tab
wget https://dl.yarnpkg.com/debian/pubkey.gpg -O pubkey-yarn.gpg
cat pubkey-yarn.gpg | apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
apt update
apt install -yq yarn
apt upgrade -yq
# TODO temp old list
  #nginx ffmpeg postgresql postgresql-contrib openssl g++ make redis-server git python-dev sudo
apt install -yq certbot nginx ffmpeg postgresql postgresql-contrib openssl g++ make redis-server git cron wget

# Now that dependencies are installed, before running PeerTube you should start PostgreSQL and Redis:
systemctl start redis postgresql

# PeerTube user
useradd -m -d /var/www/peertube -s /bin/bash -p peertube peertube

# Database
sudo -iu postgres createuser peertube
sudo -iu postgres psql -U postgres postgres <<EOF
 ALTER USER "peertube" WITH PASSWORD 'peertube' ;
EOF
sudo -iu postgres createdb -O peertube -E UTF8 -T template0 peertube_prod
sudo -iu postgres psql -c "CREATE EXTENSION pg_trgm;" peertube_prod
sudo -iu postgres psql -c "CREATE EXTENSION unaccent;" peertube_prod

# Prepare PeerTube directory
# VERSION=$(curl -s https://api.github.com/repos/chocobozzz/peertube/releases/latest | grep tag_name | cut -d '"' -f 4) && echo "Latest Peertube version is $VERSION"
VERSION=v3.4.1
sudo -u peertube mkdir /var/www/peertube/config
mkdir -p "${PEERTUBE_STORAGE}"
chown -R peertube:peertube "${PEERTUBE_STORAGE}"
sudo -u peertube mkdir "${PEERTUBE_STORAGE}/storage"
sudo -u peertube mkdir /var/www/peertube/versions
  # Download the latest version of the Peertube client, unzip it and remove the zip
sudo -u peertube wget -q \
  "https://github.com/Chocobozzz/PeerTube/releases/download/${VERSION}/peertube-${VERSION}.zip" \
  -O "/var/www/peertube/versions/peertube-${VERSION}.zip"
sudo -u peertube unzip -q /var/www/peertube/versions/peertube-${VERSION}.zip -d /var/www/peertube/versions/
sudo -u peertube rm /var/www/peertube/versions/peertube-${VERSION}.zip
sudo -u peertube ln -s /var/www/peertube/versions/peertube-${VERSION} /var/www/peertube/peertube-latest
cd /var/www/peertube/peertube-latest
echo "next command is: peertube yarn install --production --pure-lockfile --silent"
sudo -H -u peertube yarn install --production --pure-lockfile --silent
cd -

# PeerTube configuration
#   https://github.com/Chocobozzz/PeerTube/blob/develop/config/default.yaml
sudo -u peertube cp /var/www/peertube/peertube-latest/config/default.yaml /var/www/peertube/config/default.yaml
# load templates according to env vars
#   https://github.com/Chocobozzz/PeerTube/raw/3edbafb6377cfb65bca3964d46fa27bc9f813300/config/production.yaml.example
cat > /var/www/peertube/config/production.yaml <<EOF
${PT_CONFIG}
EOF
#   https://github.com/Chocobozzz/PeerTube/raw/3edbafb6377cfb65bca3964d46fa27bc9f813300/support/nginx/peertube
cat > /etc/nginx/sites-available/peertube <<EOF
${NGINX_CONFIG}
EOF

# Webserver
ln -s /etc/nginx/sites-available/peertube /etc/nginx/sites-enabled/peertube
nginx -t
systemctl reload nginx

# TCP/IP Tuning
#   https://github.com/Chocobozzz/PeerTube/blob/develop/support/sysctl.d/30-peertube-tcp.conf
cp /var/www/peertube/peertube-latest/support/sysctl.d/30-peertube-tcp.conf /etc/sysctl.d/
sysctl -p /etc/sysctl.d/30-peertube-tcp.conf

# systemd - Check the service file (PeerTube paths and security directives):
cp /var/www/peertube/peertube-latest/support/systemd/peertube.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable peertube
systemctl start peertube
