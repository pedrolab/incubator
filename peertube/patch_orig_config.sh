#!/bin/sh

set -x
set -e

# WIP: this will be eventually merged to update_files

regen_diff() {
  diff -u "${src_file}" "${dst_file}" > "${patch_file}" || true

  # override diff header
  sed -i '1,2d' "${patch_file}"
  sed -i "1i --- ${target_file}" "${patch_file}"
  sed -i "1i +++ ${target_file}" "${patch_file}"
}


main() {
  src_file='production.yaml.orig'
  dst_file='production.yaml.sh'
  patch_file='production.yaml.patch'
  target_file='production.yaml.sh.test'

  if [ "${REGEN_DIFF}" ]; then
    regen_diff
  fi
  cp  "${src_file}" "${target_file}"
  # src https://unix.stackexchange.com/questions/423186/diff-how-to-ignore-empty-lines
  patch --merge -i "${patch_file}"
}

main "${@}"
