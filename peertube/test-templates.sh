#!/bin/sh -e

set -u
. ./myenv

. ./production.yaml.sh
cat > production.yaml <<EOF
$PT_CONFIG
EOF

. ./nginx.sh
cat > nginx <<EOF
$NGINX_CONFIG
EOF

set +u
